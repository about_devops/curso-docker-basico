import requests,os,sys

key = os.getenv('key')
value = os.getenv('value')
if key == None or value == None:
    print('set environment key and value')
    print('example key=name value=john python3 set_key.py')
    sys.exit()

data = {"key":key,"value":value}
headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
url = "http://127.0.0.1/set"
url_test = "http://127.0.0.1/"
test = requests.get(url_test)
try:
    if test.status_code == 200:
        r = requests.post(url=url, headers=headers, json=data)
        print(r.json())
except:
    print('api down')
    sys.exit()
