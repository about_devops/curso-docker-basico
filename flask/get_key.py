import requests,os,sys

key = os.getenv('key')
if key == None:
    print("set variable environment key")
    print("example key=name")
    sys.exit()

url = "http://127.0.0.1/get"
url_test = "http://127.0.0.1"
data = {"key":key}
headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
test = requests.get(url_test)
try:
    if test.status_code == 200:
        r = requests.post(url=url, headers=headers, json=data)
        print(r.text)
except:
    print('api down')
    sys.exit()
